﻿using EShop.Domain.Domain;

using EShop.Service.Interface;

using Microsoft.AspNetCore.Mvc;
using Movie_App.Service.Interface;

namespace EShop.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IOrderService _orderService;

        private readonly IConcertService _concertService;
        public AdminController(IOrderService orderService, IConcertService concertService)
        {
            _orderService = orderService;   
            _concertService = concertService;
        }
        [HttpGet("[action]")]
        public List<Order> GetAllOrders()
        {
            return this._orderService.GetAllOrders();
        }
        [HttpGet("{id}/export-pdf")]
        public IActionResult ExportOrderToPdf(BaseEntity id)
        {
            
            string fileName = $"order_{id}.pdf";
            string filePath = Path.Combine(Path.GetTempPath(), fileName);

            
            _orderService.ExportOrderById(id, filePath);

           
            if (System.IO.File.Exists(filePath))
            {
                
                var fileStream = System.IO.File.OpenRead(filePath);
                return File(fileStream, "application/pdf", fileName);
            }
            else
            {
                
                return NotFound();
            }
        }
        [HttpPost("[action]")]
        public Order GetDetailsForOrder(BaseEntity id)
        {
            var data = this._orderService.GetDetailsForOrder(id);
            return data;
        }

        [HttpPost("[action]")]
        public bool ImportAllConcerts(List<Concert> model)
        {
            bool status = true;

            foreach (var item in model)
            {
                    var concert = new Concert
                    { 
                        ConcertName = item.ConcertName,
                        ConcertDescription = item.ConcertDescription,
                        ConcertImage = item.ConcertImage,
                        Rating = item.Rating
                    };

                    _concertService.CreateNewConcert(concert);
                    status = true;
               
            }
            return status;
        }
    }
}
