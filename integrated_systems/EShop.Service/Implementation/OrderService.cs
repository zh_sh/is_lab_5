﻿using EShop.Domain.Domain;
using EShop.Repository.Interface;
using EShop.Service.Interface;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Service.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void ExportOrderById(BaseEntity id, string filePath)
        {
            var order = _orderRepository.GetDetailsForOrder(id);

            
            iTextSharp.text.Document document = new iTextSharp.text.Document();
            PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
            document.Open();

            
            document.Add(new Paragraph($"Order ID: {order.Id}"));
            document.Add(new Paragraph($"Owner ID: {order.OwnerId}"));
            

            
            foreach (var productInOrder in order.ProductInOrders)
            {
                document.Add(new Paragraph($"Product ID: {productInOrder.OrderedProduct.Id}"));
                document.Add(new Paragraph($"Price: {productInOrder.OrderedProduct.Price}"));
                document.Add(new Paragraph($"Quantity: {productInOrder.Quantity}"));
                
                document.Add(new Paragraph("\n")); 
            }

            document.Close();
        }

        public List<Order> GetAllOrders()
        {
            return _orderRepository.GetAllOrders();
        }

        public Order GetDetailsForOrder(BaseEntity id)
        {
            return _orderRepository.GetDetailsForOrder(id);
        }
    }
}
