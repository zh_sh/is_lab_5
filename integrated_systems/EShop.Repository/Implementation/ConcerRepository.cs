﻿using EShop.Domain.Domain;
using EShop.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShop.Repository.Implementation
{
    public class ConcerRepository : IConcertRepository
    {
        private readonly ApplicationDbContext context;
        private DbSet<Concert> entities;

        public ConcerRepository(ApplicationDbContext context)
        {
            this.context = context;
            entities = context.Set<Concert>();
        }
        public List<Concert> GetAllOrders()
        {
            return entities.ToList();
        }

        public Concert GetDetailsForOrder(Guid id)
        {
            return entities.SingleOrDefault(c => c.Id == id);
        }
    }
}
